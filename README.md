# Erply Go Middleware API
Simple API Server for Erply Skill Test

This documentation provides walkthrough to the implementation of Erply's middleware API written in Go.<br>
This covers some of the basic implementation of the official [api-go-wrapper](https://github.com/erply/api-go-wrapper) for Erply's APIs.

The project is written following Uncle Bob's [clean architecture](https://gist.github.com/ygrenzinger/14812a56b9221c9feca0b3621518635b) using Module-Oriented Structure (kinda inspired by DDD) that makes Server-Oriented Architecture projects easy to decompase into Fine-Grained Microservices Architecture.

```bash
Project Structure

├── app
│   ├── http
│   │   ├── middlewares
│   │   │       └── VerifyIdentityToken.go
│   │   └── viewmodels
│   │   │       └── HTTPResponseVM.go
│   ├── modules
│   │   ├── auth
│   │   │   ├── http
│   │   │   │   └── AuthController.go
│   │   │   ├── types
│   │   │   │   └── AuthRequest.go
│   │   │   │   └── AuthResponse.go
│   │   │   │   └── AuthServiceInterface.go
│   │   │   └── AuthService.go
│   │   ├── customers
│   │   │   ├── http
│   │   │   │   └── CustomerController.go
│   │   │   ├── types
│   │   │   │   └── CustomerRequest.go
│   │   │   │   └── CustomerServiceInterface.go
│   │   │   └── CustomerService.go
│   ├── providers
│   │   ├── erply
│   │   │   ├── api.go
│   │   │   └── api_test.go
│   ├── utils
│   │   ├── helpers
│   │   │   ├── math
│   │   │   │   └── conversion.go
│   │   │   │   └── conversion_test.go
│   ├── docs
│   │   ├── Insomnia_2020-04-24.json
│   ├── infrastructures
│   │   ├── databases
│   │   │   ├── redis
│   │   │   │   └── types
│   │   │   │   │   └── RedisDBHandlerInterface.go
│   │   │   │   └── RedisDBHandler.go
│   │   │   │   └── RedisDBHandler_test.go
│   │   ├── sdk
│   │   │   ├── erply
│   │   │   │   └── types
│   │   │   │   │   └── ErplyAPIHandlerInterface.go
│   │   │   │   └── ErplyAPIHandler.go
│   │   │   │   └── ErplyAPIHandler_test.go
├── .env.example
├── .gitignore
├── go.mod
├── go.sum
├── main.go
├── README.md
├── router.go
└── servicecontainer.go
```
This project structure is inspired by https://github.com/kabaluyot/gomora

## System Requirements
To run the project, the following dependencies must be installed in your system:
* go ~1.13 (https://golang.org/)
* redis (https://redis.io)
* (optional) nodeJS (https://nodejs.org) - needed to run a simple web server for API docs

## Environment Variables
Before running the project, configure the following:
```bash
$ cp .env.example .env
```

Then add the following configs in your .env file:
```bash
API_NAME=erply-interview
API_ENV=local
API_URL=http://localhost
API_URL_PORT=                 # this defaults to 8000 if not set

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=
REDIS_PORT=6379
REDIS_DB_JWT=0                # redis db index, 0 (default) is used for cached jwt
```

## Installing Dependencies
We are using Go Modules (https://blog.golang.org/using-go-modules) in this project, to install dependencies, one of the recommended ways is to:
```bash
$ go mod tidy
```
This command actually pulls depedency and get rid of unused dependencies.

For the vendor. run:
```bash
$ go mod vendor
```
## Starting the Development Server
Like any other Go projects, build the binary and run using:
```bash
$ go build && ./erply-interview
```
Again, the default address for this is at: http://localhost:8000

## Running Test
You may easily invoke **ALL** the native Go test using the following command,
```bash
$ go test -v ./...
```
> Note: For the following test file, you must assign the following keys (these are dynamic):
```go
ErplyAPIHandler_test.go

const (
	sessionKey string = "ea4e1eab60749e0a99ad02d48cc32c6345c63ad808b6"  // replace with current sessionKey
	clientCode string = "110397"    // replace with current clientCode
	jwt        string = "eryrdtfdgfdgfdgfdgfdg.dgf" // place identity token (jwt) here
)
```
```go
api_test.go (erply-interview/app/providers/erply)

credentials := UserCredentials{
    ClientCode: 110397,                    // write your client code here
    Username:   "sovietkamarov@gmail.com", // write your username here
    Password:   "demo1234",                // write your password here
}

```

## Running the API Documentation Server
The project is using the insomnia-documenter (https://github.com/jozsefsallai/insomnia-documenter) for all the REST API routes.

To run, go to the docs directory:
```bash
$ cd docs
```
And run the following:
```bash
$ npx insomnia-documenter --config Insomnia_2020-04-24.json
$ npx serve
```
By default, visit https://localhost:5000 for the documentation.

> Note: Because the insomnia json file containing the json structure is included, you may easily import this to an Insomnia client or Postman client (or other http clients that can parse it) to access it interactively.

## Issues
Some of the issues encountered while developing the project includes:
* Cannot fully proxy pass the response from the Erply API when using Erply's API Go Wrapper (the responses are lacking compared to the base endpoint)

## TODO
The project is not yet perfect, below are possible enhancements and recommendations:
* Apply mock testing to all modules
* Static godoc (See open issue https://github.com/golang/go/issues/2381)
* Add Docker configuration for containerized development

## Contributors
[![](https://avatars0.githubusercontent.com/u/38805756?s=90&u=96545a7174420f0ae00a9511c74e6ed74a9e5319&v=4)](https://github.com/kabaluyot)