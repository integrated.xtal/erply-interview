package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	viewmodels "github.com/kabaluyot/erply-interview/app/http/viewmodels"
	auth_types "github.com/kabaluyot/erply-interview/app/modules/auth/types"
	redis_types "github.com/kabaluyot/erply-interview/infrastructures/databases/redis/types"
	erply_types "github.com/kabaluyot/erply-interview/infrastructures/sdk/erply/types"
)

// VerifyIdentityToken handles http request authentication provided by the IdentityToken (Authorization)
// See router.go for the list of routes protected by this mididleware.
type VerifyIdentityToken struct {
	erply_types.ErplyAPIHandlerInterface
	redis_types.RedisDBHandlerInterface
}

// Verify authenticates API request with valid identity token (JWT)
// The sessionKey and clientCode is retrieve from redis cached to update the ErplyHandler Client
func (middleware *VerifyIdentityToken) Verify(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		identityToken := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", -1)

		if len(identityToken) < 10 {
			// unauthorized access
			response := viewmodels.HTTPResponseVM{
				Code:    http.StatusUnauthorized,
				Success: false,
				Status:  "Token not found or expired",
			}

			response.JSON(w)
			return
		}

		// retrieve required data for API request
		var cachedData auth_types.CacheAuthData

		data, err := middleware.RedisDBHandlerInterface.Get(identityToken)
		if err != nil {
			// server error
			response := viewmodels.HTTPResponseVM{
				Code:    http.StatusInternalServerError,
				Success: false,
			}

			response.JSON(w)
			return
		}

		// cast to struct
		if err := json.Unmarshal([]byte(data), &cachedData); err != nil {
			// server error
			response := viewmodels.HTTPResponseVM{
				Code:    http.StatusUnauthorized,
				Success: false,
				Status:  "Token not found or expired",
			}

			response.JSON(w)
			return
		}

		// set erply api client
		middleware.ErplyAPIHandlerInterface.NewErplyAPIService(cachedData.SessionKey, fmt.Sprintf("%d", cachedData.ClientCode))

		// make the request
		cachedData.SessionKey, err = middleware.ErplyAPIHandlerInterface.VerifyIdentityToken(identityToken)
		if err != nil {
			// unauthorized access∂
			response := viewmodels.HTTPResponseVM{
				Code:    http.StatusUnauthorized,
				Success: false,
				Status:  "Token not found or expired",
			}

			response.JSON(w)
			return
		}

		// update current session key
		cacheData, _ := json.Marshal(cachedData)

		err = middleware.RedisDBHandlerInterface.Set(identityToken, cacheData)
		if err != nil {
			response := viewmodels.HTTPResponseVM{
				Code:    http.StatusInternalServerError,
				Success: false,
			}

			response.JSON(w)
			return
		}

		next.ServeHTTP(w, r)
		return
	})
}
