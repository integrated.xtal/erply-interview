package auth

import (
	"encoding/json"

	auth_types "github.com/kabaluyot/erply-interview/app/modules/auth/types"
	"github.com/kabaluyot/erply-interview/app/providers/erply"
	redis_types "github.com/kabaluyot/erply-interview/infrastructures/databases/redis/types"
	erply_types "github.com/kabaluyot/erply-interview/infrastructures/sdk/erply/types"
)

// AuthService handles the service logic invoked by the AuthController
type AuthService struct {
	erply_types.ErplyAPIHandlerInterface
	redis_types.RedisDBHandlerInterface
}

// VerifyUser handles the VerifyUser logic for user authentication
func (service *AuthService) VerifyUser(credentials auth_types.VerifyUserState) (erply.ErplyAPIResponse, error) {
	data := erply.UserCredentials{
		ClientCode: credentials.ClientCode,
		Username:   credentials.Username,
		Password:   credentials.Password,
	}
	res, err := erply.VerifyUser(data)
	if err != nil {
		return res, err
	}

	// convert dict to struct response
	var record auth_types.VerifyUserRecordResponse

	recordDict := res.Records[0].(map[string]interface{})
	recordJSON, _ := json.Marshal(recordDict)

	if err := json.Unmarshal(recordJSON, &record); err != nil {
		return res, err
	}

	// save identity token data
	cacheData, _ := json.Marshal(auth_types.CacheAuthData{
		ClientCode: credentials.ClientCode,
		SessionKey: record.SessionKey,
		UserName:   record.UserName,
	})

	err = service.RedisDBHandlerInterface.Set(record.IdentityToken, cacheData)
	if err != nil {
		return res, err
	}

	return res, nil
}
