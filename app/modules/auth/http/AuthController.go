package http

import (
	"encoding/json"
	"net/http"

	viewmodels "github.com/kabaluyot/erply-interview/app/http/viewmodels"
	"github.com/kabaluyot/erply-interview/app/modules/auth/types"
)

// AuthController handles incoming http request for the auth module
type AuthController struct {
	types.AuthServiceInterface
}

// VerifyUser handles user authentication (login) to retrieve the identityToken, sessionKey, and user info.
func (controller *AuthController) VerifyUser(w http.ResponseWriter, r *http.Request) {
	var request types.VerifyUserState

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusInternalServerError,
			Success: false,
		}

		response.JSON(w)
		return
	}

	// basic validation
	if len(request.Username) == 0 || len(request.Password) == 0 {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusUnauthorized,
			Success: false,
		}

		response.JSON(w)
		return
	}

	resp, err := controller.AuthServiceInterface.VerifyUser(request)
	if err != nil {
		response := viewmodels.HTTPResponseVM{
			Status:  resp.Status,
			Code:    http.StatusUnauthorized,
			Success: false,
		}

		response.JSON(w)
		return
	}

	response := viewmodels.HTTPResponseVM{
		Records: resp.Records,
		Status:  resp.Status,
		Code:    http.StatusOK,
		Success: true,
	}

	response.JSON(w)
	return
}
