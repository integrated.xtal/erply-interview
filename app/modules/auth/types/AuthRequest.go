package types

// VerifyUserState holds the required fields for the VerifyUser method in AuthController
type VerifyUserState struct {
	ClientCode int    `json:"clientCode"`
	Username   string `json:"username"`
	Password   string `json:"password"`
}

// CacheAuthData holds the required fields for the redis cache
// This will be used to inject the sessionKey and clientCode in VeridyIdentityToken middleware
type CacheAuthData struct {
	ClientCode int    `json:"clientCode"`
	SessionKey string `json:"sessionKey"`
	UserName   string `json:"userName"`
}
