package types

// VerifyUserRecordResponse holds response fields for the VerifyUser method
type VerifyUserRecordResponse struct {
	IdentityToken string `json:"identityToken"`
	SessionKey    string `json:"sessionKey"`
	UserName      string `json:"userName"`
}
