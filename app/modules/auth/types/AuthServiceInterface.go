package types

import (
	"github.com/kabaluyot/erply-interview/app/providers/erply"
)

// AuthServiceInterface holds the list of methods for the AuthService
type AuthServiceInterface interface {
	VerifyUser(credentials VerifyUserState) (erply.ErplyAPIResponse, error)
}
