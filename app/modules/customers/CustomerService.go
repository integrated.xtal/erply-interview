package customers

import (
	"errors"

	customer_types "github.com/kabaluyot/erply-interview/app/modules/customers/types"
	"github.com/kabaluyot/erply-interview/infrastructures/sdk/erply/types"
)

// CustomerService handles the service logic for the customer module
type CustomerService struct {
	types.ErplyAPIHandlerInterface
}

// GetCustomerByID handles the logic for the retrieveal of customer information by ID
func (service *CustomerService) GetCustomerByID(customerID string) (types.Customer, error) {
	var customerIDs []string

	customerIDs = append(customerIDs, customerID)

	customers, err := service.ErplyAPIHandlerInterface.GetCustomersByIDs(customerIDs)
	if err != nil {
		return nil, err
	} else if len(customers) == 0 {
		return nil, errors.New("empty")
	}

	return &customers[0], nil
}

// SaveCustomer handles the logic for the creation of customer resource
func (service *CustomerService) SaveCustomer(data customer_types.SaveCustomerState) (types.CustomerImport, error) {
	customerData := types.CustomerConstructor

	customerData.CompanyName = data.CompanyName
	customerData.Address = data.Address
	customerData.PostalCode = data.PostalCode
	customerData.FullName = data.FullName
	customerData.RegistryCode = data.RegistryCode
	customerData.Email = data.Email

	res, err := service.ErplyAPIHandlerInterface.PostCustomer(&customerData)
	if err != nil {
		return nil, err
	}

	return res, nil
}
