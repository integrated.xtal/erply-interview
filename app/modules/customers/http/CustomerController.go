package http

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	viewmodels "github.com/kabaluyot/erply-interview/app/http/viewmodels"
	"github.com/kabaluyot/erply-interview/app/modules/customers/types"
)

// CustomerController handles the http requests for the customer module
// See router.go for the list of routes that are using this controller
type CustomerController struct {
	types.CustomerServiceInterface
}

// GetCustomerByID handles the retrieval of consumer resource by ID.
func (controller CustomerController) GetCustomerByID(w http.ResponseWriter, r *http.Request) {
	customerID := chi.URLParam(r, "id")

	if len(customerID) == 0 {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusInternalServerError,
			Success: false,
		}

		response.JSON(w)
		return
	}

	resp, err := controller.CustomerServiceInterface.GetCustomerByID(customerID)
	if err != nil {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusInternalServerError,
			Success: false,
			Status:  resp,
		}

		response.JSON(w)
		return
	} else if resp == nil {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusNotFound,
			Success: false,
			Status:  resp,
		}

		response.JSON(w)
		return
	}

	response := viewmodels.HTTPResponseVM{
		Records: resp,
		Code:    http.StatusOK,
		Success: true,
	}

	response.JSON(w)
	return
}

// SaveCustomer handles the creation of customer resource
// The request payload must satisfy the SaveConsumerState
func (controller *CustomerController) SaveCustomer(w http.ResponseWriter, r *http.Request) {
	var request types.SaveCustomerState

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusInternalServerError,
			Success: false,
			Status:  "Customer body is lacking required fields",
		}

		response.JSON(w)
		return
	}

	resp, err := controller.CustomerServiceInterface.SaveCustomer(request)
	if err != nil {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusInternalServerError,
			Success: false,
			Status:  resp,
		}

		response.JSON(w)
		return
	}

	response := viewmodels.HTTPResponseVM{
		Records: resp,
		Code:    http.StatusCreated,
		Success: true,
	}

	response.JSON(w)
	return
}
