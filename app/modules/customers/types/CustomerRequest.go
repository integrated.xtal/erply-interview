package types

// SaveCustomerState holds the request fields for SaveConsumer method from CustomerController
type SaveCustomerState struct {
	CompanyName  string `json:"companyName"`
	Address      string `json:"address"`
	PostalCode   string `json:"postalCode"`
	FullName     string `json:"fullName"`
	RegistryCode string `json:"registryCode"`
	Email        string `json:"email"`
}
