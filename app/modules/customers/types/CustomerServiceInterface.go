package types

import (
	"github.com/kabaluyot/erply-interview/infrastructures/sdk/erply/types"
)

// CustomerServiceInterface holds the list of methods for the CustomerService
type CustomerServiceInterface interface {
	GetCustomerByID(customerID string) (types.Customer, error)
	SaveCustomer(data SaveCustomerState) (types.CustomerImport, error)
}
