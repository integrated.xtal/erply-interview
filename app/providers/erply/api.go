package erply

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

// UserCredentials holds the user credential fields
type UserCredentials struct {
	ClientCode int
	Username   string
	Password   string
}

// ErplyAPIResponse holds the erply api response fields
type ErplyAPIResponse struct {
	Status  MetaStatusResponse `json:"status"`
	Records []interface{}      `json:"records"`
}

// MetaStatusResponse is a sub struct that holds the fields for ErplyAPIResponse Status
type MetaStatusResponse struct {
	Request           string  `json:"request"`
	RequestUnixTime   int     `json:"requestUnixTime"`
	ResponseStatus    string  `json:"responseStatus"`
	ErrorCode         int     `json:"errorCode"`
	GenerationTime    float32 `json:"generationTime"`
	RecordsTotal      int     `json:"recordsTotal"`
	RecordsInResponse int     `json:"recordsInResponse"`
}

var (
	client *http.Client = &http.Client{Timeout: 10 * time.Second}
)

// VerifyUser serves as the proxy method that forwards the verify user request type to the erply API
// This is essential since the official Go API Wrapper for Erply API does not support the VerifyUser endpoint yet
func VerifyUser(credentials UserCredentials) (ErplyAPIResponse, error) {
	var response ErplyAPIResponse

	req, err := http.NewRequest("POST", fmt.Sprintf("https://%d.erply.com/api/", credentials.ClientCode), nil)

	params := url.Values{}
	params.Add("request", "verifyUser")
	params.Add("clientCode", fmt.Sprintf("%d", credentials.ClientCode))
	params.Add("username", credentials.Username)
	params.Add("password", credentials.Password)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return response, err
	}
	defer resp.Body.Close()

	if _ = json.NewDecoder(resp.Body).Decode(&response); response.Status.ResponseStatus == "error" {
		return response, errors.New("Invalid credentials")
	}

	return response, nil
}
