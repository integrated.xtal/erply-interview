package erply

import (
	"testing"
)

func TestVerifyUser(t *testing.T) {
	credentials := UserCredentials{
		ClientCode: 110397,                    // write your client code here
		Username:   "sovietkamarov@gmail.com", // write your username here
		Password:   "demo1234",                // write your password here
	}

	_, err := VerifyUser(credentials)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(true)
}
