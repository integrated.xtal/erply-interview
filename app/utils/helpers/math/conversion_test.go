package math

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConversion(t *testing.T) {
	t.Run("test tofloat32", func(t *testing.T) {
		assert.Equal(t, float32(5.52), ToFloat32("5.52"))
	})

	t.Run("test tofloat64", func(t *testing.T) {
		assert.Equal(t, 5.52, ToFloat64("5.52"))
	})

	t.Run("test int32", func(t *testing.T) {
		assert.Equal(t, int32(5), ToInt32("5"))
	})

	t.Run("test int64", func(t *testing.T) {
		assert.Equal(t, int64(5), ToInt64("5"))
	})

	t.Run("test touint32", func(t *testing.T) {
		assert.Equal(t, uint32(5), ToUint32("5"))
	})

	t.Run("test touint64", func(t *testing.T) {
		assert.Equal(t, uint64(5), ToUint64("5"))
	})
}
