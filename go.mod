module github.com/kabaluyot/erply-interview

go 1.13

require (
	github.com/erply/api-go-wrapper v0.0.1
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/stretchr/testify v1.5.1
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
	gopkg.in/inf.v0 v0.9.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
