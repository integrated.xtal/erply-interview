package erply

import (
	"errors"

	"github.com/erply/api-go-wrapper/pkg/api"
)

// ErplyAPIHandler handles the methods for the ErplyAPI Go SDK
type ErplyAPIHandler struct {
	Client api.IClient
}

// NewErplyAPIService creates a new client provided by sessionKey and  clientCode
func (s *ErplyAPIHandler) NewErplyAPIService(sessionKey, clientCode string) {
	s.Client = api.NewClient(sessionKey, clientCode, nil)
}

// GetIdentityToken retrieves the current identity token
func (s ErplyAPIHandler) GetIdentityToken() (string, error) {
	res, err := s.Client.GetIdentityToken()
	if err != nil {
		return "", err
	}

	return res.Jwt, nil
}

// GetCustomersByIDs retrieves the customers records provided by customeer ids (array)
func (s ErplyAPIHandler) GetCustomersByIDs(customerIDs []string) (api.Customers, error) {
	res, err := s.Client.GetCustomersByIDs(customerIDs)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// PostCustomer creates a customer resource
func (s ErplyAPIHandler) PostCustomer(data *api.CustomerConstructor) (*api.CustomerImportReport, error) {
	res, err := s.Client.PostCustomer(data)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// VerifyIdentityToken validates the authenticity of identity token (jwt)
// This returns an updated session key that can be used to refresh the current key
func (s ErplyAPIHandler) VerifyIdentityToken(jwt string) (string, error) {
	res, err := s.Client.VerifyIdentityToken(jwt)
	if err != nil {
		return "", err
	}

	if res == nil {
		return "", errors.New("session key is empty")
	}

	return res.SessionKey, nil
}
