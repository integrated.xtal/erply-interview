package erply

import (
	"testing"

	"github.com/erply/api-go-wrapper/pkg/api"
)

const (
	sessionKey string = "ea4e1eab60749e0a99ad02d48cc32c6345c63ad808b6"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               // replace with current sessionKey
	clientCode string = "110397"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     // replace with current clientCode
	jwt        string = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ0X2lzcyI6bnVsbCwidF9wdCI6IjYwN2ExYTBhOWQ1NDU1YTc1N2EwNTM4Y2ViYTMzYTQ5MmIxNWM1YWMiLCJ0X2lzc3QiOjE1ODc3MDg3MjUsInRfcnQiOiJiM2E1ZWFkMGIyNTkzOTgzZDU3YmY5MGJhYzZjZDQ1MjQwY2IyY2VhIiwidF90dGwiOjg2NDAwLCJpZF9lbSI6InNvdmlldGthbWFyb3ZAZ21haWwuY29tIiwiaWRfdGlkIjoiMTEwMzk3IiwidF92IjoyLCJ0X2l0diI6eyJlcnBseSI6IjExMDM5NyIsImVycGx5LXVzZXJuYW1lIjoic292aWV0a2FtYXJvdkBnbWFpbC5jb20ifSwicGVybXMiOlsiQ0ROXC9tYW5hZ2UtcmVzb3VyY2VzIiwiYmFjay1vZmZpY2VcL2JhY2stb2ZmaWNlLWFjY2VzcyJdfQ.teQJulNFA_KNMoauXQvEmcKxuNyAMpGYkp1_zhj-9MQspkAqGHOwnidu-WJrl0VekKP9S9eHrH4fxQJNiAY1RtLi26Gl7y81-XUCHRDoXOlPtz41jySuwGRrHQIOE2F2As9DqPnhneSD0p6Pzb4TYNu3NrhAvG2fV8KoFTEzAgocw6RUxoVosK8rS_Yy1iOyy866Cs6c5dNnRFQNmv3EY_ZfhxUrUM6_6FPzv_lYsR-aaTkv_jMohOme2RsQvl8UkCVadw7xak782CYibBQIP4JOuK9T2lWsKtAHRKKA7cpN52e01CVKvcVW_R0SEmsmvcWsAygv0XukStHjVz2WRw" // place identity token (jwt) here
)

func TestVerifyIdentityToken(t *testing.T) {
	cli := &ErplyAPIHandler{}

	cli.NewErplyAPIService(sessionKey, clientCode)

	resp, err := cli.VerifyIdentityToken(jwt)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(resp)
}

func TestGetIdentityToken(t *testing.T) {
	cli := &ErplyAPIHandler{}

	cli.NewErplyAPIService(sessionKey, clientCode)

	resp, err := cli.GetIdentityToken()
	if err != nil {
		t.Error(err)
		return
	}

	if len(resp) == 0 {
		t.Error("token is empty")
		return
	}

	t.Log(resp)
}

func TestGetCustomersByIDs(t *testing.T) {
	cli := &ErplyAPIHandler{}

	cli.NewErplyAPIService(sessionKey, clientCode)

	resp, err := cli.GetCustomersByIDs([]string{"33"}) // list customer ids here
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(resp)
}

func TestPostCustomer(t *testing.T) {
	cli := &ErplyAPIHandler{}

	cli.NewErplyAPIService(sessionKey, clientCode)

	customerData := &api.CustomerConstructor{
		CompanyName:  "Erply Company 1",
		Address:      "Philippines",
		PostalCode:   "8000",
		FullName:     "Karl",
		RegistryCode: "test 1234",
		Email:        "test@email.com",
	}

	resp, err := cli.PostCustomer(customerData)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(resp)
}
