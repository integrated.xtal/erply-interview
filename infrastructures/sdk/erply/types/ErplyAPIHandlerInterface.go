package types

import (
	"github.com/erply/api-go-wrapper/pkg/api"
)

// ErplyAPIHandlerInterface holds the list of implementable methods for the ErplyAPIHandler
type ErplyAPIHandlerInterface interface {
	NewErplyAPIService(sessionKey, clientCode string)
	GetCustomersByIDs(customerIDs []string) (api.Customers, error)
	PostCustomer(data *api.CustomerConstructor) (*api.CustomerImportReport, error)
	VerifyIdentityToken(jwt string) (string, error)
}
