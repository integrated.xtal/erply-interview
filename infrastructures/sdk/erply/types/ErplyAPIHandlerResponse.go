package types

import (
	"github.com/erply/api-go-wrapper/pkg/api"
)

// Customer holds the Customer response type
type Customer *api.Customer

// CustomerImport holds the CustomerImport response type
type CustomerImport *api.CustomerImportReport
