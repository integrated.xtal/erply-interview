/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| This file contains the routes mapping and groupings of your REST API calls.
| See README.md for the routes UI server.
|
*/
package main

import (
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	viewmodels "github.com/kabaluyot/erply-interview/app/http/viewmodels"
)

// ChiRouterInterface chi router interface
type ChiRouterInterface interface {
	InitRouter() *chi.Mux
}

type router struct{}

var (
	m          *router
	routerOnce sync.Once
)

// InitRouter initializes main routes
func (router *router) InitRouter() *chi.Mux {
	verifyIdentityTokenMiddleware := ServiceContainer().RegisterVerifyIdentityTokenMiddleware()
	authController := ServiceContainer().RegisterAuthController()
	customerController := ServiceContainer().RegisterCustomerController()

	r := chi.NewRouter()

	// global and recommended middlewares
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Recoverer)

	// default route
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		response := viewmodels.HTTPResponseVM{
			Code:    http.StatusUnauthorized,
			Success: false,
		}

		response.JSON(w)
		return
	})

	r.Group(func(r chi.Router) {
		r.Route("/api", func(r chi.Router) {
			r.Route("/v1", func(r chi.Router) {
				// public routes
				r.Route("/auth", func(r chi.Router) {
					r.Post("/verify-user", authController.VerifyUser)
				})

				// protected routes
				r.Group(func(r chi.Router) {
					r.Use(verifyIdentityTokenMiddleware.Verify)

					r.Route("/customer", func(r chi.Router) {
						r.Post("/", customerController.SaveCustomer)
						r.Get("/{id}", customerController.GetCustomerByID)
					})
				})
			})
		})
	})

	return r
}

// ChiRouter export instantiated chi router once
func ChiRouter() ChiRouterInterface {
	if m == nil {
		routerOnce.Do(func() {
			m = &router{}
		})
	}
	return m
}
