/*
|--------------------------------------------------------------------------
| Service Container
|--------------------------------------------------------------------------
|
| This file performs the compiled dependency injection for your middlewares,
| controllers, services, providers, repositories, etc..
|
*/
package main

import (
	"fmt"
	"os"
	"sync"

	middlewares "github.com/kabaluyot/erply-interview/app/http/middlewares"
	"github.com/kabaluyot/erply-interview/app/modules/auth"
	auth_http "github.com/kabaluyot/erply-interview/app/modules/auth/http"
	"github.com/kabaluyot/erply-interview/app/modules/customers"
	customer_http "github.com/kabaluyot/erply-interview/app/modules/customers/http"
	"github.com/kabaluyot/erply-interview/infrastructures/databases/redis"
	"github.com/kabaluyot/erply-interview/infrastructures/sdk/erply"

	"github.com/kabaluyot/erply-interview/app/utils/helpers/math"
)

// ServiceContainerInterface contains the dependency injected instances
type ServiceContainerInterface interface {
	RegisterVerifyIdentityTokenMiddleware() middlewares.VerifyIdentityToken
	RegisterAuthController() auth_http.AuthController
	RegisterCustomerController() customer_http.CustomerController
}

type kernel struct{}

var (
	k               *kernel
	containerOnce   sync.Once
	erplyAPIHandler *erply.ErplyAPIHandler
	redisDBHandler  *redis.RedisDBHandler
)

func init() {
	erplyAPIHandler = &erply.ErplyAPIHandler{}

	// init redis cache
	redisDBHandler = &redis.RedisDBHandler{}
	_, err := redisDBHandler.Connect(fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")), os.Getenv("REDIS_PASSWORD"), int(math.ToInt32(os.Getenv("REDIS_DB_JWT"))))
	if err != nil {
		panic(err)
	}
}

// RegisterVerifyIdentityTokenMiddleware performs dependency injection to the VerifyIdentityToken middleware
func (k *kernel) RegisterVerifyIdentityTokenMiddleware() middlewares.VerifyIdentityToken {
	verifyIdentityTokenMiddleware := middlewares.VerifyIdentityToken{
		ErplyAPIHandlerInterface: erplyAPIHandler,
		RedisDBHandlerInterface:  redisDBHandler,
	}

	return verifyIdentityTokenMiddleware
}

// RegisterAuthController performs dependency injection to the AuthController
func (k *kernel) RegisterAuthController() auth_http.AuthController {
	authService := &auth.AuthService{
		ErplyAPIHandlerInterface: erplyAPIHandler,
		RedisDBHandlerInterface:  redisDBHandler,
	}
	authController := auth_http.AuthController{
		AuthServiceInterface: authService,
	}

	return authController
}

// RegisterCustomerController performs dependency injection to the CustomerController
func (k *kernel) RegisterCustomerController() customer_http.CustomerController {
	customerService := &customers.CustomerService{
		ErplyAPIHandlerInterface: erplyAPIHandler,
	}
	customerController := customer_http.CustomerController{
		CustomerServiceInterface: customerService,
	}

	return customerController
}

// ServiceContainer export instantiated service container once
func ServiceContainer() ServiceContainerInterface {
	if k == nil {
		containerOnce.Do(func() {
			k = &kernel{}
		})
	}
	return k
}
